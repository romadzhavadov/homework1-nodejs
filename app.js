const http = require('http');
const commandLineArgs = require('command-line-args');

console.log(process.argv);

const optionDefinitions = [
  { name: 'port', alias: 'p', type: Number },
];

const options = commandLineArgs(optionDefinitions);

// const PORT = process.argv[2] || 3000;
const PORT = options.port || 3000;
let requestCount = 0;
console.log(requestCount)

const server = http.createServer((req, res) => {
  requestCount++;
  res.setHeader('Content-Type', 'text/html');
  res.write(JSON.stringify({ message: 'Request handled successfully', requestCount })); 
  console.log(requestCount);
  res.end();
});

server.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
